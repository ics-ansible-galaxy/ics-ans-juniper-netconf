# ics-ans-juniper-netconf

Ansible playbook to enable netconf and add a network super-user for juniper switches.

## License

BSD 2-clause
